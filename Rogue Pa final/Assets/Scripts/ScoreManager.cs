﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	private LifeManager lifeSystem;

	public static int score;

	Text text;

	void Start()
	{
		text = GetComponent<Text> ();
		lifeSystem = FindObjectOfType<LifeManager> ();

		score = 0;
	}

	void Update()
	{
		if (score < 0)
			score = 0;

		text.text = "" + score;
	}
	
	public static void AddPoints (int pointsToAdd)
	{
		score += pointsToAdd;
	}

	public static void Reset()
	{
		score = 0;
	}

}
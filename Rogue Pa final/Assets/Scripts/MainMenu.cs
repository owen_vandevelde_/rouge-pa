﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public string startLevel;
	public string levelSelect;
	public string tutorialLevel;

	public void NewGame()
	{
		Application.LoadLevel (startLevel);
	}

	public void LevelSelect()
	{
		Application.LoadLevel (levelSelect);
	}

	public void Tutorial()
	{
		Application.LoadLevel (tutorialLevel);
	}

	public void QuitGame()
	{
		Debug.Log ("Quit");
		Application.Quit ();
	}

}
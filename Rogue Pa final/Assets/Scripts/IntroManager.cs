﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IntroManager : MonoBehaviour {

	private PlayerController player;

	Text text;

	// Use this for initialization
	void Start () {
	
		text = GetComponent<Text> ();

	}
	
	// Update is called once per frame
	void Update () {
	
		if (player.transform.position.x > 10f) {

			text.text = "Now Press W to jump and A to move backwards.";

		}

	}
}
